const mongoose = require("mongoose");

(async () => {
  //Connect to mongodb server
  await mongoose.connect("mongodb://localhost:27017/binaracademy", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  //Init mongoose schema
  const Schema = mongoose.Schema;
  //Create Schema Users (Migrations)
  const Users = new Schema({
    fullName: String,
    age: Number,
  });

  const UsersModel = mongoose.model("users", Users);

  const data = [
    { fullName: "Putin", age: 30 },
    { fullName: "Trump", age: 34 },
  ];
  // Initiate allowed keys
  const allowedkeys = ["fullName", "age"];

  try {
    // Pengulangan untuk mencari key yang tidak sesuai
    for (let index = 0; index < data.length; index++) {
      const requestKeys = Object.keys(data[index]);
      for (let index2 = 0; index2 < requestKeys.length; index2++) {
        if (!allowedkeys.includes(requestKeys[index2])) {
          throw new Error(`data ${requestKeys[index2]} tidak sesuai format`);
        }
      }
    }
    // Insert new Data
    await UsersModel.create(data);
    console.log("succes insert data to users schema");
  } catch (error) {
    console.log(error);
  }
})();
