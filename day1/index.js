const mongoose = require("mongoose");

(async () => {
  //Connect to mongodb server
  await mongoose.connect("mongodb://localhost:27017/binaracademy", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  //Init mongoose schema
  const Schema = mongoose.Schema;
  //Create Schema Users (Migrations)
  const Users = new Schema({
    fullName: String,
    age: Number,
  });

  const UsersModel = mongoose.model("users", Users);

  // Insert new Data
  //await UsersModel.create({ fullName: "John Mayer", age: 34 });

  //Get existing data
  //   const data = await UsersModel.find({});
  //   console.log(data);

  //Update data
  //   await UsersModel.updateOne(
  //     { _id: "600e8758777b91127f164e8a" },
  //     { fullName: "Mika" },
  //   );

  //delete data
  // await UsersModel.deleteOne({ _id: "600e8758777b91127f164e8a" });

  // const data = await UsersModel.find({});
  // console.log(data);
})();
