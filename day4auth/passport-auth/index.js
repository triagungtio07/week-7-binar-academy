const express = require("express");
const bodyParser = require("body-parser");
const expressSession = require("express-session");
const app = express();
const passport = require("passport");
const mongoose = require("mongoose");
const passportLocalMongoose = require("passport-local-mongoose");
const connectEnsureLogin = require("connect-ensure-login");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  expressSession({
    secret: "Agun9!glints",
    resave: false,
    saveUninitialized: false,
  })
);

app.use(passport.initialize());
app.use(passport.session());

// mongoose.connect("mongodb://localhost:27017/authpassport", {
//   useNewUrlParser: true,
//   useUnifiedTopology: true,
// });

const Schema = mongoose.Schema;

const UsersSchema = new Schema({
  username: String,
  password: String,
});

UsersSchema.plugin(passportLocalMongoose);

const UsersModel = mongoose.model("users", UsersSchema);

passport.use(UsersModel.createStrategy());
passport.serializeUser(UsersModel.serializeUser());
passport.deserializeUser(UsersModel.deserializeUser());

//Routes

app.post("/register", (req, res) => {
  UsersModel.register(
    { username: req.body.username, active: false },
    req.body.password
  );
  // UserDetails.register({ username: "jay", active: false }, "jay");
  // UserDetails.register({ username: "roy", active: false }, "roy");

  res.send("succes register users");
});

app.get("/login", (req, res) => res.send(" you must login before"));

app.post("/login", (req, res, next) => {
  passport.authenticate("local", (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.redirect("/404");
    }
    req.logIn(user, (err) => {
      if (err) {
        return next(err);
      }

      return res.redirect("/users");
    });
  })(req, res, next);
});

app.get("/404", (req, res) => res.send("you're not authenticated"));

/*
  ensureLoggedin secara default mengarahkan ke "/login"
  jika ingin mengarahkan ke alamat lain bise menggunakan {redirectTo: "/yangdiinginkan"}
*/
app.get("/users", connectEnsureLogin.ensureLoggedIn(), (req, res) =>
  res.send("congrats, you're authenticated")
);

app.listen(3000, () => console.log("this app running on port 3000"));
