const mongoose = require("mongoose");

(async () => {
  //Connect to mongodb server
  await mongoose.connect("mongodb://localhost:27017/binaracademy", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  //Init mongoose schema
  const Schema = mongoose.Schema;
  //Create Schema Users (Migrations)
  const Users = new Schema({
    fullName: String,
    age: Number,
  });

  const UsersModel = mongoose.model("users", Users);

  // Insert new Data
  await UsersModel.create([
    { fullName: "Durterte", age: 34 },
    { fullName: "Jokowi", age: 30 },
    { fullName: "Angela Markel", age: 20 },
    { fullName: "Trudeou", age: 25 },
    { fullName: "Joe Biden", age: 38 },
    { fullName: "Mahatir", age: 30 },
    { fullName: "Xi Jin Ping", age: 34 },
    { fullName: "Putin", age: 30 },
    { fullName: "Trump", age: 34 },
    { fullName: "Kim Jong Un", age: 30 },
  ]);
})();
