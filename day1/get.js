const mongoose = require("mongoose");

(async () => {
  //Connect to mongodb server
  await mongoose.connect("mongodb://localhost:27017/binaracademy", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  //Init mongoose schema
  const Schema = mongoose.Schema;
  //Create Schema Users (Migrations)
  const Users = new Schema({
    fullName: String,
    age: Number,
  });

  const UsersModel = mongoose.model("users", Users);

  //Get existing data
  const data = await UsersModel.find({});
  console.log(data);
})();
