const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

mongoose.connect("mongodb://localhost:27017/authjwt", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const Schema = mongoose.Schema;
const UsersSchema = new Schema({
  username: String,
  password: String,
  salt: String,
});

const UsersModel = mongoose.model("users", UsersSchema);

const app = express();
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: false }));

//middleware login authentifikasi

const middlewareAuth = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, "bin4r4c4demy");

    res.locals.user = decodedToken;
    next();
  } catch (error) {
    res.status(400).json({ message: "token invalid" });
  }
};

/**
 * Routing Login
 * Routing ini berguna untuk proses login dari users
 */
app.post("/login", async (req, res) => {
  let statusCode = 500;

  try {
    const username = req.body.username;
    const password = req.body.password;
    //Cari data di mongodb dengan username dari req.body
    const users = await UsersModel.findOne({ username: username });

    //melakukan pengecekan apakah data username ada atau tidak

    if (!users) {
      statusCode = 404;
      throw new Error("users not found");
    }

    const isPasswordMatch = await bcrypt.compare(password, users.password);
    if (!isPasswordMatch) {
      statusCode = 400;
      throw new mongoose.Error("Password invalid");
    }
    //Generate token JWT(note: bin4r4c4demy diubah sesuai selera)
    const token = jwt.sign({ username: users.username }, "bin4r4c4demy");
    res.json({
      status: 200,
      message: "Succes Login",
      data: { token: token },
    });
  } catch (error) {
    res.status(statusCode).json({ message: error.message });
  }
});

/**
 * Routing register
 * Routing ini berguna unutuk menambah data users ke database
 */

app.post("/register", async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const saltKey = await bcrypt.genSalt(10);
  const hashPassword = await bcrypt.hash(password, saltKey);

  //memasukkan data ke database
  await UsersModel.create({
    username: username,
    password: hashPassword,
    salt: saltKey,
  });

  res.json({
    message: "Succces insert new users",
  });
});

/**
 * Routing yang memerlukan proses authentifikasi
 * example: Routing untuk get semua data users dalam
 */

app.get("/users", middlewareAuth, (req, res) => {
  res.json({ message: "succes get profile", data: res.locals.user });
});

app.listen(5000, () => console.log("this app running on port on 5000"));
