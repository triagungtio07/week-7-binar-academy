const mongoose = require("mongoose");

(async () => {
  //Connect to mongodb server
  await mongoose.connect("mongodb://localhost:27017/binaracademy", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  //Init mongoose schema
  const Schema = mongoose.Schema;
  //Create Schema Users (Migrations)
  const Users = new Schema({
    fullName: String,
    age: Number,
  });

  const UsersModel = mongoose.model("users", Users);

  //Update data
  await UsersModel.updateOne({ _id: "600fb72db3333a3a5d1a3abc" }, { age: 100 });
})();
